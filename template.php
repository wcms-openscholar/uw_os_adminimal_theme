<?php

/**
 * @file
 * Here we override the default HTML output of drupal.
 */

/**
 * Override or insert variables into the html template.
 */
function uw_os_adminimal_theme_preprocess_html(&$vars) {

  // Get folder path.
  $path = drupal_get_path('theme', 'uw_os_adminimal_theme');

  // Add default styles in a way that will put them *after* the Adminimal styles.
  drupal_add_css($path . '/css/admin.css', array('group' => CSS_THEME, 'media' => 'all', 'weight' => 2));

  drupal_add_js($path . '/js/uw_os_adminimal_theme.js', 'file');

  // Add defined suffix to all page titles. Default to University.
  if (isset($vars['head_title'])) {
    $vars['head_title'] .= ' | University of Waterloo';
  }
}
