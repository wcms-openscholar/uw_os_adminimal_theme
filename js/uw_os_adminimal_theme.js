/**
 * @file
 */

(function ($) {
  Drupal.behaviors.uw_os_adminimal_theme = {
    attach: function (context, settings) {
      var data_titles = [];
      var counter = 0;
      $("thead th").each(function (index) {
        data_titles.push($(this).text());
        if ($(this).attr('colspan')) {
          for (i = 1; i < $(this).attr('colspan'); i++) {
            data_titles.push("");
          }
        }
        counter++;
      });

      var dt_index = 0;
      var dt_length = data_titles.length / 2;

      $("td").each(function (index) {
        if (!$(this).attr('colspan')) {
          if (dt_index < dt_length) {
            $(this).attr("data-title", data_titles[dt_index]);
          }
          dt_index++;
          if (dt_index >= dt_length) {
            dt_index = 0;
          }
        }
      });
    }

  };
})(jQuery);
